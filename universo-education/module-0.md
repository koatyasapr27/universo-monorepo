# Модуль 0 — Подготовка

> **ВНИМАНИЕ!**
>
> **Этот материал ещё неполный, он находится в разработке и регулярно обновляется.**

- Требования к железу, ОС и другому программному обеспечению находятся в разработке.

- Установить Visual Studio Code https://code.visualstudio.com/

- Поставить в VSC разные плагину по Vue, Quasar в том числе сниппеты по Pinia. Список находится в разработке.

- Установить Яндекс Браузер https://browser.yandex.ru/

- Установить Vue 3 devtools, актуальную ссылку на расширение для вашего браузера можно найти на официальном сайте https://devtools.vuejs.org/guide/installation.html

- Установить в браузер расширение Web Developer, для Яндекс Браузера и других браузеров на основе Chromium https://chrome.google.com/webstore/detail/web-developer/bfbameneiokkgbdmiekhjnmfkcnldhhm

Для Firefox https://addons.mozilla.org/ru/firefox/addon/web-developer/
