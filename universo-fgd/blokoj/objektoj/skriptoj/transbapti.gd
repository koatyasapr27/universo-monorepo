extends "res://kerno/fenestroj/tipo_c1.gd"

# transbapti - переименовать (изменить наименование)
# poprigi - применить
# nuligi - отменить
# по́дпись subskribo


const QueryObject = preload("res://blokoj/objektoj/skriptoj/queries.gd")


var objekto = null


func _on_nuligi_pressed():
	self.set_visible(false)


func _on_proprigi_pressed():
	var q = QueryObject.new()
	Net.send_json(q.sxanghi_nomo_objekto(objekto['uuid'], $tipo_c/subskribo_nomo/nomo.text))
	self.set_visible(false)


func aldoni_objekto(aldoni_obj):
	objekto = aldoni_obj.duplicate()
	$tipo_c/subskribo_nomo/nomo.text = objekto['nomo']['enhavo']

