extends "res://kerno/fenestroj/tipo_a1.gd"


var konservejo # склад
var konservejo_aktuala # текущая выделенная строка

# serĉo - поиск
func sercxo_konservejo(sxipo):
	# нужно работать с direktebla_objekto
	if not sxipo.get('ligilo'):
		return null
	for ligilo in sxipo['ligilo']['edges']:
		if ligilo['node']['ligilo']['resurso']['objId'] == 5 or \
			ligilo['node']['ligilo']['resurso']['objId'] == 19:
#			if Global.logs:
#				print('===склад ==',ligilo['node']['ligilo'])
			return ligilo['node']['ligilo']
	return null


func load_konservejo():
	if !Global.fenestro_kosmo:
		print('konservejo - нет космоса')
		return
	var sxipo = Global.fenestro_kosmo.get_node('ship')
	if not sxipo:
		return
	# находим склад
#	konservejo = sercxo_konservejo(sxipo)
	konservejo = sercxo_konservejo(Global.direktebla_objekto[Global.realeco-2])
	if !konservejo:
		print('упс, склад не нашли :-(')
		return
	FillItemList()


const item_konservejo = preload("res://blokoj/objektoj/scenoj/ITEM_konservejo.tscn")

@onready var vbox = get_node("VBox/body_texture/ScrollContainer/VBoxContainer")
@onready var bottom = get_node("VBox/HBox_bottom/bottom_texture1/HBoxContainer")


# очищаем окно 
func clear_item():
	for ch in vbox.get_children():
		ch.free()


# выводим, что на складе 
func FillItemList():
	if !konservejo:
		return
	clear_item()
	var i = 1
	var volumeno = 0
	var item = item_konservejo.instantiate()
	# заполняем заголовки столбцов
	item.aldoni_kolumno(false,'','col1', self)
	item.aldoni_kolumno(false,'внутренний объём','col2', self)
	item.aldoni_kolumno(false,'внешний объём','col3', self)
	item.aldoni_kolumno(false,'объём хранения','col4', self)
	item.disconnect("gui_input", Callable(item, '_on_ITEM_konservejo_gui_input'))
	item.modulate = Color(0.4, 0.6, 1, 0.862745)
	vbox.add_child(item)
	# enteno - содержа́ние (одной субстанции в другой)
	if konservejo.get('ligilo'):
		for enteno in konservejo['ligilo']['edges']:
			if enteno['node']['tipo']['objId']==3: # Находится внутри
				item = item_konservejo.instantiate()
				item.aldoni_kolumno(false,str(i) + ')' +
					String(enteno['node']['ligilo']['nomo']['enhavo']),'col1', self)
				item.aldoni_kolumno(false,String(enteno['node']['ligilo']['volumenoInterna']),'col2', self)
				item.aldoni_kolumno(false,String(enteno['node']['ligilo']['volumenoEkstera']),'col3', self)
				item.aldoni_kolumno(false,String(enteno['node']['ligilo']['volumenoStokado']),'col4', self)
				vbox.add_child(item)
				volumeno += enteno['node']['ligilo']['volumenoStokado']
				i += 1
	if konservejo['volumenoInterna']:
		bottom.get_node('all').text = String(konservejo['volumenoInterna'])
		bottom.get_node('free').text = String(konservejo['volumenoInterna'] - volumeno)
#		_items.add_item('Всего места: ' + String(konservejo['volumenoInterna']))
#		_items.add_item('Свободно: ' + String(konservejo['volumenoInterna'] - volumeno))


@onready var vb = get_node("VBox/body_texture/TEST2/ScrollContainer/VBoxContainer")
	

func _on_Button_pressed():
	var item = item_konservejo.instantiate()
	# заполняем заголовки столбцов
	item.aldoni_kolumno(false,'Npp','col1', self)
	item.aldoni_kolumno(false,'внутренний объём','col2', self)
	item.aldoni_kolumno(false,'внешний объём','col3', self)
	item.aldoni_kolumno(false,'объём хранения','col4', self)
	item.disconnect("gui_input", Callable(item, '_on_ITEM_konservejo_gui_input'))
	item.modulate = Color(0.4, 0.6, 1, 0.862745)
	vb.add_child(item)
	for i in range(4):# заполняем строки
		item = item_konservejo.instantiate()
		item.aldoni_kolumno(false,'col-' + str(i) + ')','col1', self)
		item.aldoni_kolumno(false,'col2=' + str(i),'col2', self)
		item.aldoni_kolumno(false,'col3=' + str(i),'col3', self)
		item.aldoni_kolumno(false,'col4=' + str(i),'col4', self)
		vb.add_child(item)

